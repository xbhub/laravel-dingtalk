<?php


namespace Xbhub\Dingtalk\Api\Cspace;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Exceptions\ClientError;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    public function grant_custom_space()
    {
        $res = $this->app['http_client']->get('cspace/get_custom_space', [
            'query' => [
                'access_token' => $this->app['credential']->token(),
                'agent_id' => '191227346',
                'domain' => 'demo',
            ]
        ]);

        return json_decode($res->getBody()->getContents(), true);
    }

}
