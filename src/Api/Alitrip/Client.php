<?php


namespace Xbhub\Dingtalk\Api\Alitrip;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{

    /**
     * 差旅申请用户搜索可用发票列表
     *
     * @param [type] $corpid
     * @param [type] $userid
     * @param string $title
     * @return void
     */
    public function invoiceSearch($userid, $title = '')
    {
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.invoice.search', [
            'rq' => json_encode([
                'title' => $title,
                'userid' => $userid,
                'corpid' => config('dingtalk.corp_id'),
            ])
        ]);
    }

    /**
     * 查询成本中心
     *
     * @param [type] $corpid
     * @return void
     */
    public function costCenterQuery($userid)
    {
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.cost.center.query', [
            'rq' => json_encode([
                'corpid' => config('dingtalk.corp_id'),
                'userid' => $userid
            ])
        ]);
    }

    /**
     * 获取绑定的淘宝账号信息
     *
     * @param [type] $corpid
     * @param [type] $userid
     * @return void
     */
    public function bindTaobalGet($userid)
    {
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.address.get', [
            'request' => json_encode([
                'corpid' => config('dingtalk.corp_id'),
                'userid' => $userid
            ])
        ]);
    }

    /**
     * 用户新建审批单
     * http://open.taobao.com/api.htm?docId=38730&docType=2
     *
     * @param [type] $corpid 企业id
     * @param [type] $userid 用户id
     * @param [type] $thirdpart_apply_id 外部申请单id
     * @param [type] $status 审批单状态，不传入默认为0：0审批中，1同意，2拒绝
     * @param [type] $trip_title  申请单标题
     * @param [type] $trip_cause 出差事由
     * @param [type] $itinerary_list 行程列表
     * @param [type] $traveler_list 出行人列表
     * @return void
     */
    public function ApprovalNew($userid, $thirdpart_apply_id, $status, $trip_title, $trip_cause, $itinerary_list, $traveler_list, $options = [])
    {
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.approval.new', [
            'rq' => json_encode(array_merge([
                'corpid' => config('dingtalk.corp_id'),
                'userid' => $userid,
                'thirdpart_apply_id' => $thirdpart_apply_id,
                'status' =>$status,
                'trip_title' => $trip_title,
                'trip_cause' => $trip_cause,
                'itinerary_list' => $itinerary_list,
                'traveler_list' => $traveler_list
            ], $options))
        ]);
    }

    /**
     * 更新审批单
     * http://open.taobao.com/api.htm?docId=38840&docType=2
     *
     * @param [type] $corpid
     * @param [type] $userid
     * @param [type] $thirdpart_apply_id
     * @param [type] $status 1已同意 2已拒绝 3已转交 4已取消
     * @param [type] $operate_time
     * @param array $options
     * @return void
     */
    public function ApprovalUpdate($userid, $thirdpart_apply_id, $status, $operate_time = '', $options = [])
    {
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.approval.update', [
            'rq' => json_encode(array_merge([
                'corpid' => config('dingtalk.corp_id'),
                'userid' => $userid,
                'thirdpart_apply_id' => $thirdpart_apply_id,
                'status' =>$status,
                'operate_time' => $operate_time ?: now()->format('Y-m-d H:i:s')
            ], $options))
        ]);
    }

    /**
     * 获取单个申请单的详情数据
     * http://open.taobao.com/api.htm?docId=38778&docType=2
     *
     * @param [type] $corpid
     * @param [type] $apply_id
     * @param array $options
     * @return void
     */
    public function ApprovalGet($apply_id, $options = [])
    {
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.apply.get', [
            'rq' => json_encode(array_merge([
                'corpid' => config('dingtalk.corp_id'),
                'apply_id' => $apply_id
            ], $options))
        ]);
    }


    /**
     * 查询接口
     * http://open.taobao.com/api.htm?docId=38807&docType=2
     * http://open.taobao.com/api.htm?docId=38810&docType=2
     * http://open.taobao.com/api.htm?docId=38808&docType=2
     * http://open.taobao.com/api.htm?docId=38876&docType=2
     *
     * @param [type] $type   vehicle|hotel|flight|train
     * @param [type] $corpid
     * @param [type] $userid
     * @param [type] $start_time
     * @param [type] $end_time
     * @param [type] $apply_id
     * @param integer $page_size
     * @param array $options
     * @return void
     */
    public function OrderSearch($type, $userid, $start_time, $end_time, $apply_id, $page_size = 50, $options = [])
    {
        $methods = ['vehicle', 'hotel', 'flight', 'train'];
        if(!in_array($type, $methods)) abort(500, 'type not support');
        return $this->httpPostMethod('dingtalk.oapi.alitrip.btrip.'.$type.'.order.search', [
            'rq' => json_encode(array_merge([
                'corpid' => config('dingtalk.corp_id'),
                'userid' => $userid,
                'start_time' => $start_time,
                'end_time' => $end_time,
                'apply_id' => $apply_id,
                'page_size' => $page_size
            ], $options))
        ]);
    }


    /**
     * 获取商旅访问地址
     * https://open.taobao.com/api.htm?docId=48569&docType=2
     *
     * @param [type] $corpid
     * @param [type] $userid
     * @param integer $type 类目类型：1：机票，2：火车票，3：酒店，4：用车
     * @param integer $action_type 操作类型：1：预订，2：我的订单列表
     * @return void
     */
    public function addressGet($userid, $type, $action_type, $itinerary_id, $phone)
    {
        return $this->httpGetMethod('dingtalk.oapi.alitrip.btrip.address.get', [
            'request' => json_encode([
                'corpid' => config('dingtalk.corp_id'),
                'userid' => $userid,
                'type' => $type,
                'action_type' => $action_type,
                'itinerary_id' => $itinerary_id,
                'phone' => $phone
            ])
        ]);
    }


}
