<?php

/*
 * This file is part of the xbhub/dingtalk.
 *
 * (c) jory <jorycn@163.com>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Xbhub\Dingtalk\Api\Message;

use Xbhub\Dingtalk\Api\Kernel\BaseClient;
use Xbhub\Dingtalk\Api\Kernel\Messages\Message;

/**
 * Class Client.
 *
 * @author jory <jorycn@163.com>
 */
class Client extends BaseClient
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param string $messageId
     *
     * @return array
     */
    public function status(string $messageId)
    {
        return $this->httpPostJson('message/list_message_status', compact('messageId'));
    }

    /**
     * @param array|null $data
     *
     * @return array
     */
    public function send(array $data = null)
    {
        return $this->httpPostJson('topapi/message/corpconversation/asyncsend_v2', $data ?? $this->data);
    }

    /**
     * @param string|array $user
     *
     * @return $this
     */
    public function toUser($user)
    {
        $this->data['touser'] = implode('|', (array) $user);

        return $this;
    }

    /**
     * @param string|array $party
     *
     * @return $this
     */
    public function toParty($party)
    {
        $this->data['toparty'] = implode('|', (array) $party);

        return $this;
    }

    /**
     * @param int $agent
     *
     * @return $this
     */
    public function ofAgent(int $agent)
    {
        $this->data['agentid'] = $agent;

        return $this;
    }

    /**
     * @param $message
     *
     * @return $this
     */
    public function withReply($message)
    {
        $this->data += Message::parse($message)->transform();

        return $this;
    }
}
