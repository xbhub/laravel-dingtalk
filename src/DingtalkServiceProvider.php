<?php

namespace Xbhub\Dingtalk;

use Illuminate\Support\ServiceProvider;
use Xbhub\Dingtalk\Api\Application;

class DingtalkServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/route.php');
        // 发布配置
        $source = realpath(__DIR__ . '/../config/dingtalk.php');
        $this->publishes([
            $source => config_path('dingtalk.php'),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('dingtalk', function ($app) {
            $config = config('dingtalk');
            return new Application($config);
        });
    }

    public function provides()
    {
        return ['dingtalk'];
    }
}