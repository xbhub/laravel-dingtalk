<?php
namespace Xbhub\Dingtalk\Http\Controllers;

use Illuminate\Http\Request;
use Xbhub\Dingtalk\Utils\Crypto;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Xbhub\Dingtalk\Events\UserLeave;
use Xbhub\Dingtalk\Events\UserModify;
use Illuminate\Support\Facades\Artisan;
use Xbhub\Dingtalk\Events\ProcessUpdate;
use Xbhub\Dingtalk\Events\DepartmentDelete;
use Xbhub\Dingtalk\Events\DepartmentModify;

/**
 * Class VotesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CallbackController extends Controller
{

    /**
     * @throws \Xbhub\Dingtalk\Utils\Exception
     */
    public function recevie()
    {
        $signature = $_GET["signature"];
        $timeStamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];
        $postdata = file_get_contents("php://input");
        $postList = json_decode($postdata,true);
        $encrypt = $postList['encrypt'];
        $msg = "";

        try {
            $_config = config('dingtalk');
            $crypto = new Crypto($_config['token'], $_config['aes_key'], $_config['corp_id']);
            $ret = $crypto->decryptMsg($signature, $timeStamp, $nonce, $encrypt);
        } catch (Exception $e) {
            Log::error("DecryptMsg Exception".$e->getMessage());
            print $e->getMessage();
            exit();
        }

        $eventMsg = json_decode($ret, true);
        $eventType = $eventMsg['EventType'];

        // $ret['UserId'],$ret['DeptId']为数组
        switch ($eventType){
            case "user_add_org":
            case "user_modify_org":
                //通讯录用户增加 do something
                if(isset($eventMsg['UserId'])) {
                    event(new UserModify($eventMsg['UserId']));
                }
                // Log::warning("【callback】:user_modify_org|user_add_org".$ret);
                break;
            case "user_leave_org":
                //通讯录用户离职  do something
                if(isset($eventMsg['UserId'])) {
                    event(new UserLeave($eventMsg['UserId']));
                }
                // Log::warning("【callback】:user_leave_org".$ret);
                break;
            case "org_admin_add":
                //通讯录用户被设为管理员 do something
//                Artisan::call('biu:sync_user access');
                Log::info("【callback】:org_admin_add_action".$ret);
                break;
            case "org_admin_remove":
                //通讯录用户被取消设置管理员 do something
//                Artisan::call('biu:sync_user access');
                Log::info("【callback】:org_admin_remove_action".$ret);
                break;
            case "org_dept_create":
            case "org_dept_modify":
                //通讯录企业部门修改 do something
                if(isset($eventMsg['DeptId'])) {
                    event(new DepartmentModify($eventMsg['DeptId']));
                }
                break;
            case "org_dept_remove":
                //通讯录企业部门删除 do something
                if(isset($eventMsg['DeptId'])) {
                    event(new DepartmentDelete($eventMsg['DeptId']));
                }
                break;
            case "org_remove":
                //企业被解散 do something
                Log::info("【callback】:org_remove_action".$ret);
                break;
            case "bpms_task_change":
            case "bpms_instance_change":
                if(isset($eventMsg['processInstanceId'])) {
                    event(new ProcessUpdate($eventMsg['processInstanceId']));
                }
                break;
            case "check_url"://do something
            default : //do something
                break;
        }

        $res = "success";
        $encryptMsg = "";
        $errCode = $crypto->encryptMsg($res, $timeStamp, $nonce, $encryptMsg);

        echo $errCode;
    }

}
