<?php

return [
    'host'  => 'https://oapi.dingtalk.com',
    'corp_id'     => env('DINGTALK_CROP_ID',''),
  
    'app_id'       => env('DINGTALK_APPID',''),
    'app_secret'   => env('DINGTALK_APPSECRET',''),
    'redirect_uri' => env('DINGTALK_REDIRECTURI',''),
];
